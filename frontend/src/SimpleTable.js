import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from "axios";

import Button from '@material-ui/core/Button';

const styles = theme => ({
    //simpletable
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    //button
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});


let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];

function fetchData(success) {
    console.log('getting request to contacts...');
    return axios.get('http://localhost:8080/contacts')
        .then(response => {console.log(response);return response.data}).then(success);
}
function deleteData(id) {
    console.log('deleting request to contacts...');
    return axios.delete('http://localhost:8080/contacts/'+id)
        .then(response => {console.log(response);});
}



class SimpleTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formData: {
                email: '',
                password: '',
                repeatPassword: '',
                birth: '',
            },
            submitted: false,
            data:[],
        };

    }

    componentDidMount() {
        //setInterval(this.hello, 5000);
        fetchData((data)=>{this.setState({data:data});console.log(data.length);});

    }

    handleClick=(id)=>{
       deleteData(id);
    };

    render() {
        const { classes } = this.props;
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Username</TableCell>
                            <TableCell align="right">Country</TableCell>
                            <TableCell align="right">delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.data.map(row => {
                            return (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.name}
                                    </TableCell>
                                    <TableCell align="right">{row.address}</TableCell>
                                    <TableCell align="right">{row.address}</TableCell>
                                    <TableCell align="right"> <Button variant="contained" type="submit" onClick={this.handleClick(row.id)}>
                                        Delete
                                    </Button></TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);
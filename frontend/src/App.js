import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";
//For material UI
import Button from '@material-ui/core/Button';
import SimpleTable from './SimpleTable';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
});
//axios
//https://github.com/axios/axios
function signOut() {
    console.log('Posting request to API...');
    let data={name:"imname", address:"imaddress"};
    axios.post('/contacts', {data})
        .then((result) => {
            console.log(result);
            //this.props.history.push("/")
        });
}
//body:JSON.stringify()
class App extends Component {

    state = {};

    componentDidMount() {
        setInterval(this.hello, 5000);
    }

    hello = () => {
        let data={name:"imname", address:"imaddress"};
        /*
        axios.post('http://localhost:8080/contacts', {data})
            .then((response) => {
                console.log(response);
                //this.props.history.push("/")
            });
            */
        /*
        axios.get('http://localhost:7070/kafka-music/charts/genre/pop')
            .then(response => console.log(response));
*/
        axios.get('http://localhost:8080/hello')
            .then(response => console.log(response));
        axios.get('http://localhost:8080/contacts')
            .then(response => console.log(response));
    };

    handleClick=()=>{
        signOut();
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">{this.state.message}</h1>
                </header>
                <p className="App-intro">
                    Hello !To get started, edit <code>src/App.js</code> and save to reload.
                </p>
                <SimpleTable/>

            </div>
        );
    }
}

export default App;


/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
*/

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";

import App from './App';
import ComposedTextField from './ComposedTextField';

import AutoGrid from './AutoGrid';

ReactDOM.render(
    <Router>
        <div>
            <Route exact path='/' component={App} />


            {/*
            <Route path='/table' component={SimpleTable} />
            <Route path='/grid' component={AutoGrid} />
            <Route path="/edit" render={(props) => <Edit {...props}/>}/>
            <Route path="/public" render={(props) => <Public {...props}/>}/>
            <PrivateRoute path="/protected" component={Protected} />
            */}
        </div>
    </Router>,
    document.getElementById('root')
);
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';

//for materialUI
import Button from '@material-ui/core/Button';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

function signUp(data) {
    console.log('Posting request to API...');
    fetch('http://localhost:8080/handlelogin', {
        method: 'post',
        body: JSON.stringify(data)
    }).then(function(response) {
        return response.json();
    }).then(function() {
        console.log('posted');
    });
}

class ComposedTextField extends React.Component {
    state = {
        username: '',
        password:'',
    };

    componentDidMount() {
        this.forceUpdate();
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    handleClick=()=>{
        signUp(this.state);
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.container}>
                <form name="signinform" onSubmit={this.handleClick}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="component-simple">Email</InputLabel>
                    <Input id="component-simple" value={this.state.username} onChange={this.handleChange('username')} required/>
                </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="component-simple">password</InputLabel>
                        <Input id="component-simple" value={this.state.password} onChange={this.handleChange('password')} required/>
                    </FormControl>
                <Button variant="contained" type="submit">
                    Sign In
                </Button>
                </form>
            </div>
        );
    }
}

ComposedTextField.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ComposedTextField);
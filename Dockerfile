#./gradlew build first!!
# . after ADD needed?? YES!!

#how to push
#docker build --tag=[Dockerhubユーザ名]/myfirstimage .
#docker login
#docker push [Dockerhubユーザ名]/myfirstimages

##shold add two space before ADD second argument

FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD ./build/libs/actuator-sample-0.0.1-SNAPSHOT.jar   app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-Dspring.data.mongodb.uri=mongodb://mongo/mydatabase", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

#VOLUME holding externally mounted volumes from the native host
#ADD copies the executable JAR generated during the build to the container root directory. 
#ENTRYPOINT defines the command to execute when the container is started.
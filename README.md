# Develop and deploy Kubernetes applications with Docker

**Team member**
- HOKHY TANN
- 董宇涛
- 会川慧


 
# Prepare reusable app template
## 1. Prepare a CI/CD environment
## 2. Prepare a web app
## 3. Build container image
## 4. Prepare a Kubernetes environment
## 5. Deploy web app on Kubernetes







# 1. Prepare a CI/CD environment

# **Continuous delivery of a Spring Boot application with GitLab CI and Kubernetes**

- Creating a continuous delivery pipeline with GitLab CI

 gitlab-cli.yml
```yaml

image: docker:latest
services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: gitlab-ci

stages:
  - build
  - package
  - deploy


#
#Prepare automated frontend image build with Dockerfile
#
forntend-build:
  stage: build
  script:
  - docker build -t registry.gitlab.com/satooooshi1/khmersay-client ./khmersay/khmersay-client

#
#Prepare automated backend image build with Dockerfile
#
backend-build:
  stage: build
  script:
  - docker build -t registry.gitlab.com/satooooshi1/khmersay-server ./khmersay/khmersay-server

#
#Prepare automated image push to registry for kubernete
#
docker-build:
  stage: package
  script:
  - docker-compose build
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  - docker push registry.gitlab.com/satooooshi1/khmersay-client
  - docker push registry.gitlab.com/satooooshi1/khmersay-server

#
#
#Here should come with the kubernete deployment with  
# Google Cloud SDK
#Instead, deploy on minikube
#

```

- **front/backend-build job**  
The ront/backend-build checks front/backend run correctly. 

- **docker-build job**  
The docker-build job packages the application into a Docker container. We define package as the build stage since we need the front/backend-build job to check front/backend run correctly.



# 2. Prepare a web app
# **Packaging a NodeJs+React+MongoDB application as a Docker container**

## **App name**: Khmersay

- CSS Flexbox, Bootstrap  
- ReactJS, states and props management  
- Client-Side routing using React-Router-Dom, the new and best practices on Redirect component  
- Deliberately try not to use Redux for state management and passing props between componets  
- Make http request to back-end API using Axios.  
- Integrate third-party text Editor, TinyMCE
- Design Database Schema in MongoDB   
- Server-Side routing  
- Build RESTful API in Nodejs  

Before we can proceed to the creation of the pipeline, we need to add a couple of files to our repository to package our application as a Docker container and to describe the target deployment in Kubernetes terms.



Frontend Dockerfile

```Dockerifle   
FROM node:latest

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN npm install

ADD src /usr/src/app/src
ADD public /usr/src/app/public

# RUN npm run build # developing mode

CMD [ "npm", "start" ]
```

Backend Dockerfile
```
FROM node:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm install
COPY . /usr/src/app
EXPOSE 5000
CMD ["npm", "start"]

```


# 4.Prepare a cluster with docker compose.

docker-compose.yml
```yml
version: "2"

services:
  react:
    build: ./khmersay/khmersay-client
    ports: 
      - "3000:3000"
    links:
      - express
  express:
    build: ./khmersay/khmersay-server
    ports:
      - "5000:5000"
    links:
      - database
  database:
    image: mongo
    ports: 
      - "27017:27017"

```




# 5. Prepare a Kubernetes environment with minikube

After minikube started, run 

- **minikube start up shell**
```
#!/bin/sh
kubectl apply -f ./kubMongoVolume.yaml
kubectl apply -f ./kubMongoVolumeClaim.yaml
kubectl apply -f ./kubMongo.yaml
kubectl apply -f ./kubMongoService.yaml
kubectl apply -f ./kubAppBack.yaml
kubectl apply -f ./kubAppBackService.yaml
kubectl apply -f ./kubAppFront.yaml
kubectl apply -f ./kubAppFrontService.yaml
kubectl get services
minikube ip
minikube service web-service --url
```

The related yml file in our [gitlab repository](https://gitlab.com/satooooshi1/actuator-sample).


# 6. Deploy web app on Kubernetes
Hasn't implemented.







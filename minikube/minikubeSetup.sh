#!/bin/sh
kubectl apply -f ./kubMongoVolume.yaml
kubectl apply -f ./kubMongoVolumeClaim.yaml
kubectl apply -f ./kubMongo.yaml
kubectl apply -f ./kubMongoService.yaml
kubectl apply -f ./kubAppBack.yaml
kubectl apply -f ./kubAppBackService.yaml
kubectl apply -f ./kubAppFront.yaml
kubectl apply -f ./kubAppFrontService.yaml
kubectl get services
minikube ip
minikube service web-service --url

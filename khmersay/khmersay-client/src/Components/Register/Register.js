import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import axios from 'axios';
import './Register.css';

class Register extends Component {
	state = {
		username: '',
		email: '',
		password: '',
		password2: '',
		err: {}
	};

	register = (e) => {
		e.preventDefault();
		const newUser = {
			username: this.state.username,
			email: this.state.email,
			password: this.state.password,
			password2: this.state.password2
		};
		axios
			.post('/register', newUser)
			.then((user) => {
				document.getElementById('form').style.display = 'none';
				document.getElementById('register_success').style.display = 'block';
			})
			.catch((err) => {
				this.setState({ err: err.response.data });
				this.setError();
			});
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	setError = () => {
		const errors = this.state.err;
		if (errors) {
			if (errors.username) {
				document.getElementById('username').style.border = '1px solid red';
				document.getElementById('username_error_msg').innerText = errors.username;
			} else {
				document.getElementById('username').style.border = '1px solid grey';
				document.getElementById('username_error_msg').innerText = '';
			}

			if (errors.email) {
				document.getElementById('email').style.border = '1px solid red';
				document.getElementById('email_error_msg').innerText = errors.email;
			} else {
				document.getElementById('email').style.border = '1px solid grey';
				document.getElementById('email_error_msg').innerText = '';
			}

			if (errors.password) {
				document.getElementById('password').style.border = '1px solid red';
				document.getElementById('password_error_msg').innerText = errors.password;
			} else {
				document.getElementById('password').style.border = '1px solid grey';
				document.getElementById('password_error_msg').innerText = '';
			}

			if (errors.password2) {
				document.getElementById('password2').style.border = '1px solid red';
				document.getElementById('password2_error_msg').innerText = errors.password2;
			} else {
				document.getElementById('password2').style.border = '1px solid grey';
				document.getElementById('password2_error_msg').innerText = '';
			}
		} else {
			console.log('nth');
		}
	};

	render() {
		document.title = 'ចុះឈ្មោះ | ចង់និយាយថា';
		return (
			<div className="container">
				<div id="button">
					<Link to="/login" id="login">
						ចូលកម្មវិធី
					</Link>
					<Link to="register" id="register">
						ចុះឈ្មោះ
					</Link>
				</div>
				<form onSubmit={this.register} id="form">
					<div className="input">
						<label htmlFor="username">
							<strong>ឈ្មោះ</strong>
						</label>
						<input
							id="username"
							name="username"
							type="text"
							value={this.state.username}
							onChange={this.onChange}
						/>
						<p id="username_error_msg" />
					</div>

					<div className="input">
						<label htmlFor="email">
							<strong>អ៊ីម៉ែល</strong>
						</label>

						<input id="email" type="text" onChange={this.onChange} name="email" value={this.state.email} />
						<p id="email_error_msg" />
					</div>
					<div className="input">
						<label htmlFor="password">
							<strong>លេខសម្ងាត់</strong>
						</label>

						<input
							id="password"
							name="password"
							type="password"
							onChange={this.onChange}
							value={this.state.password}
						/>
						<p id="password_error_msg" />
					</div>
					<div className="input">
						<label htmlFor="password2">
							<strong>បញ្ជាក់លេខសម្ងាត់ម្តងទៀត!</strong>
						</label>

						<input
							id="password2"
							name="password2"
							type="password"
							onChange={this.onChange}
							value={this.state.password2}
						/>
						<p id="password2_error_msg" />
					</div>
					<input id="submit" type="submit" value="ចុះឈ្មោះ" />
				</form>

				<div id="register_success">
					<h3>
						<i className="fas fa-check" /> អ្នកបានចុះឈ្មោះដោយជោគជ័យ!
					</h3>
					<h5>អ្នកអាចចូលកម្មវិធីហើយពេលនេះ</h5>
				</div>
			</div>
		);
	}
}

export default Register;

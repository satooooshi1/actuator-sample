import React, { Component } from 'react';
import Redirect from 'react-router-dom/Redirect';
import Link from 'react-router-dom/Link';
import './Setting.css';

class Setting extends Component {
	state = {
		changeName: false,
		changePassword: false
	};
	changeName = () => {
		this.setState({ changeName: true, changePassword: false });
	};
	changePassword = () => {
		this.setState({ changePassword: true, changeName: false });
	};
	media = window.matchMedia('(max-width: 991px)');

	render() {
		if (!this.props.isLoggedIn) {
			return <Redirect to="/login" />;
		}
		if (this.state.sucess) {
			return <Redirect to="/" />;
		}
		return (
			<div className="container wrapper">
				<div className="setting">
					<p id="title">កំណត់គណនី</p>
					<hr />
					<p id="note">
						*** កម្មវិធីពួកយើងមិនទាន់ទ្រទង់ការប្តូររូបភាពផ្ទាល់ខ្លួននៅឡើយទេ។
						មុខងារនេះនឹងត្រូវបានបន្ថែមក្នុងពេលឆាប់ៗ។
					</p>
					<div id="first_row">
						<button onClick={this.changeName}>ប្តូរឈ្មោះ</button>
						<button onClick={this.changePassword}>ប្តូរលេខសម្ងាត់</button>
						<button>ប្តូររូបភាពផ្ទាល់ខ្លួន</button>
					</div>
					<p id="remark">សូមជ្រើសរើសមុខងារ</p>
					{this.state.changeName && (
						<div id="change_name">
							<input type="text" placeholder="new name here... " />
							<button>ប្តូរឈ្មោះ</button>
						</div>
					)}
					{this.state.changePassword && (
						<div id="change_password">
							<input type="text" placeholder="new password" />
							<input type="text" placeholder="confirm password" />
							<button>ប្តូរលេខសម្ងាត់</button>
						</div>
					)}
					<div id="delete_account">
						<button>លុបគណនី</button>
					</div>
				</div>

				{this.props.isLoggedIn &&
					!this.media.matches && (
						<div className="side">
							<div id="side_menu">
								<p>
									សួរស្តី
								<span id="username">
										<strong>{this.props.user.username}!</strong>
									</span>
									<span id="profile-pic">
										<img src={this.props.user.avatar} alt="profile" />
									</span>
								</p>
								<Link to="/" id="link1">
									<i className="fas fa-home" /> ទំព័រដើម
							</Link>
								<Link to="/newpost" id="link2">
									<i className="fas fa-pencil-alt" /> សរសេអត្ថបទ
							</Link>
								<Link to="/setting" id="link3">
									<i className="fas fa-cogs" /> កំណត់គណនី
							</Link>
								<Link to="/" id="link4" onClick={this.props.setLogout}>
									<i className="fas fa-sign-out-alt" /> ចាកចេញ
							</Link>
							</div>
						</div>
					)}
			</div>
		);
	}
}

export default Setting;

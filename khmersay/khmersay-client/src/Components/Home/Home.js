import React, { Component } from 'react';
import Article from '../Article/Article';
import Link from 'react-router-dom/Link';
import axios from 'axios';
import './Home.css';

class Home extends Component {
	state = {
		articles_list: []
	};
	media = window.matchMedia('(max-width: 991px)');

	constructor() {
		super();
		axios
			.get('/articles')
			.then((articles) => {
				this.setState({ articles_list: articles.data });
			})
			.catch((err) => console.log(err));
	}

	displayArticle = () => {
		const articles = this.state.articles_list;
		const article = [];
		for (let i = 0; i < articles.length; i++) {
			article.push(<Article article={articles[i]} key={articles[i].title} />);
		}
		if (article.length === 0) {
			return (
				<div id="no_article">
					<h3>
						<i className="fab fa-angellist" /> ស្វាគមន៏មកកាន់កម្មវិធី ចង់និយាយថា!
					</h3>
					<h5>ពួកយើងមិនទាន់មានអត្ថបទទេនៅពេលនេះ! អ្នកអាចចាប់ផ្តើមសរសេរនិងចែករំលែកជាមួយពួកយើង ឥលូវនេះ</h5>
				</div>
			);
		}
		return article;
	};

	render() {
		document.title = '| ខ្ញុំមានរឿងក្នុងចិត្តចង់និយាយ ... |';
		return (
			<div className="container wrapper">
				<div className="main">
					<p id="title">
						<strong>អត្ថបទ</strong>
					</p>
					<div className="content">{this.displayArticle()}</div>
				</div>
				<div className="side">
					{!this.props.isLoggedIn && (
						<React.Fragment>
							<Link to="/login" className="side_button">
								ចូលកម្មវិធី
							</Link>
							<Link to="/register" className="side_button">
								ចុះឈ្មោះ
							</Link>
						</React.Fragment>
					)}

					{this.props.isLoggedIn &&
					!this.media.matches && (
						<div id="side_menu">
							<p>
								សួរស្តី
								<span id="username">
									<strong>{this.props.user.username}!</strong>
								</span>
								<span id="profile-pic">
									<img src={this.props.user.avatar} alt="profile" />
								</span>
							</p>
							<Link to="/" id="link1">
								<i className="fas fa-home" /> ទំព័រដើម
							</Link>
							<Link to="/newpost" id="link2">
								<i className="fas fa-pencil-alt" /> សរសេអត្ថបទ
							</Link>
							<Link to="/setting" id="link3">
								<i className="fas fa-cogs" /> កំណត់គណនី
							</Link>
							<Link to="/" id="link4" onClick={this.props.setLogout}>
								<i className="fas fa-sign-out-alt" /> ចាកចេញ
							</Link>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default Home;

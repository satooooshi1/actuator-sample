import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import axios from 'axios';
import './ReadArticle.css';

class ReadArticle extends Component {
	state = {
		liked: false,
		like_num: this.props.article.like
	};
	media = window.matchMedia('(max-width: 991px)');
	showContent = () => {
		document.getElementById('content').innerHTML = this.props.article.article;
	};
	componentDidMount() {
		this.showContent();
	}
	like = () => {
		if (!this.state.liked) {
			document.getElementById('like_button').style.backgroundColor = '#369e59';
			document.getElementById('like_button').style.color = '#fff';
			this.setState({ liked: !this.state.liked, like_num: this.state.like_num + 1 });
			this.addLike();
		} else {
			document.getElementById('like_button').style.backgroundColor = '#fff';
			document.getElementById('like_button').style.color = '#369e59';
			this.setState({ liked: !this.state.liked, like_num: this.state.like_num - 1 });
			this.unlike();
		}
	};

	addLike = () => {
		const id = this.props.article._id;
		axios
			.post('/like', { id: id })
			.then((article) => {
				console.log(article.data.article);
			})
			.catch((err) => console.log(err));
	};

	unlike = () => {
		const id = this.props.article._id;
		axios
			.post('/unlike', { id: id })
			.then((article) => {
				console.log(article.data.article);
			})
			.catch((err) => console.log(err));
	};

	render() {
		const article = this.props.article;
		return (
			<div className="container wrapper">
				<div className="main">
					<h1 id="article_title">{article.title}</h1>
					<div id="info_row">
						<h6 id="author">{article.author.author_name}</h6>
						<h6 id="like_num">ចូលចិត្ត: {this.state.like_num}</h6>
						<h6 id="read_num">អាន: {article.read}</h6>
						<button id="like_button" onClick={this.like}>
							<i class="fas fa-thumbs-up" />
						</button>
					</div>

					<img src={article.image} alt="" id="content_image" />
					<div id="content" />
				</div>

				<div className="side">
					{!this.props.isLoggedIn && (
						<React.Fragment>
							<Link to="/login" className="side_button">
								ចូលកម្មវិធី
							</Link>
							<Link to="/register" className="side_button">
								ចុះឈ្មោះ
							</Link>
						</React.Fragment>
					)}

					{this.props.isLoggedIn &&
					!this.media.matches && (
						<div id="side_menu">
							<p>
								សួរស្តី
								<span id="username">
									<strong>{this.props.user.username}!</strong>
								</span>
								<span id="profile-pic">
									<img src={this.props.user.avatar} alt="profile" />
								</span>
							</p>
							<Link to="/" id="link1">
								<i className="fas fa-home" /> ទំព័រដើម
							</Link>
							<Link to="/newpost" id="link2">
								<i className="fas fa-pencil-alt" /> សរសេអត្ថបទ
							</Link>
							<Link to="/setting" id="link3">
								<i className="fas fa-cogs" /> កំណត់គណនី
							</Link>
							<Link to="/" id="link4" onClick={this.props.setLogout}>
								<i className="fas fa-sign-out-alt" /> ចាកចេញ
							</Link>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default ReadArticle;

import React, { Component } from 'react';
import Redirect from 'react-router-dom/Redirect';
import Link from 'react-router-dom/Link';
import axios from 'axios';
import { Editor } from '@tinymce/tinymce-react';
import './New_Post.css';

class NewPost extends Component {
	state = {
		content: '',
		sucess: false
	};
	handleEditorChange = (e) => {
		this.setState({ content: e.target.getContent() });
	};
	media = window.matchMedia('(max-width: 991px)');

	addNewPost = (e) => {
		e.preventDefault();
		const title = document.getElementById('newpost_title').value;
		if (title === '') {
			alert('Title cannot be empty');
			return;
		}
		if (this.state.content === '') {
			alert('Article cannot be empty');
			return;
		}
		const newpost = {
			title,
			content: this.state.content,
			author_id: this.props.user.id,
			author_name: this.props.user.username,
			category: document.getElementById('category').value,
			image: document.getElementById('image').value
		};
		axios
			.post('/newpost', newpost)
			.then((newpost) => this.setState({ sucess: true }))
			.catch((err) => console.log(err));
	};

	render() {
		if (!this.props.isLoggedIn) {
			return <Redirect to="/login" />;
		}
		if (this.state.sucess) {
			return <Redirect to="/" />;
		}
		return (
			<div className="container wrapper">
				<div className="newpost">
					<textarea name="title" id="newpost_title" cols="93" rows="1" placeholder="ចំណងជើង" />
					<div id="cate_thumnail">
						<select id="category">
							<option value="ពីនេះពីនោះ">ពីនេះពីនោះ</option>
							<option value="ជីវិត">ជីវិត</option>
							<option value="ស្នេហា">ស្នេហា</option>
							<option value="ការងារ">ការងារ</option>
							<option value="ការសិក្សា">ការសិក្សា</option>
							<option value="ថវិកា">ថវិកា</option>
						</select>
						<div id="thumnail">
							<label htmlFor="image">រូបភាពក្រប (Thumnail): </label>
							<input type="text" id="image" placeholder="url" />
						</div>
					</div>
					<Editor
						id="editor"
						placeholder="សូមចាប់ផ្តើមសរសេរអត្ថបទនៅទីនេះ"
						init={{
							plugins: 'link image code',
							toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
						}}
						onChange={this.handleEditorChange}
					/>
					<button id="post_button" onClick={this.addNewPost}>
						ដាក់ប្រកាស
					</button>
				</div>

				{this.props.isLoggedIn &&
				!this.media.matches && (
					<div className="side">
						<div id="side_menu">
							<p>
								សួរស្តី
								<span id="username">
									<strong>{this.props.user.username}!</strong>
								</span>
								<span id="profile-pic">
									<img src={this.props.user.avatar} alt="profile" />
								</span>
							</p>
							<Link to="/" id="link1">
								<i className="fas fa-home" /> ទំព័រដើម
							</Link>
							<Link to="/newpost" id="link2">
								<i className="fas fa-pencil-alt" /> សរសេអត្ថបទ
							</Link>
							<Link to="/setting" id="link3">
								<i className="fas fa-cogs" /> កំណត់គណនី
							</Link>
							<Link to="/" id="link4" onClick={this.props.setLogout}>
								<i className="fas fa-sign-out-alt" /> ចាកចេញ
							</Link>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default NewPost;

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import './Article.css';

class Article extends Component {
	state = {
		read: false,
		article: {}
	};

	readArticle = () => {
		const id = this.props.article._id;
		axios
			.post('/read', { id: id })
			.then((article) => {
				console.log(article.data.article);
			})
			.catch((err) => console.log(err));
		this.setState({ article: this.props.article });
		this.setState({ read: true });
	};

	render() {
		if (this.state.read) {
			return (
				<Redirect
					to={{
						pathname: '/article',
						state: this.state.article
					}}
				/>
			);
		}
		return (
			<div className="article" onClick={this.readArticle}>
				<img id="cover_pic" src={this.props.article.image} alt="cover" />
				<div id="information">
					<div id="basic_info">
						<div className="first-row">
							{/* <img id="profile_pic" src={this.props.article.avatar} alt="avatar" /> */}
							<p id="author">សរសេរដោយ {this.props.article.author.author_name}</p>
							{/* <p id="date">៥ថ្ងៃមុន</p> */}
						</div>
						<div className="second-row">
							<h4 id="article_title">
								<strong>{this.props.article.title}</strong>
							</h4>
						</div>
					</div>

					<div className="third-row">
						<p id="category">{this.props.article.category}</p>
						<p id="read">ចំនួនអាន: {this.props.article.read}</p>
						<p id="like">ចំនួនចូលចិត្ត: {this.props.article.like}</p>
					</div>
				</div>
			</div>
		);
	}
}

export default Article;

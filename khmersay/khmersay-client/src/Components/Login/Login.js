import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import { Redirect } from 'react-router-dom';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import './Login.css';
import Popup from 'reactjs-popup';

class Login extends Component {
	state = {
		email: '',
		password: '',
		login_success: false,
		errors: {},
		reset_password_success: false,
		user_not_found: false
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	login = (e) => {
		e.preventDefault();
		const user = {
			email: this.state.email,
			password: this.state.password
		};

		axios
			.post('/login', user)
			.then((user) => {
				this.loginSuccess();
				this.props.checkLogin();
				this.props.setUser(jwtDecode(user.data.token));
				this.setState({ login_success: true });
			})
			.catch((err) => {
				this.setState({ errors: err.response.data });
				this.setError();
			});
	};

	resetPassword = (e) => {
		if (e) e.preventDefault();
		let email = document.getElementById('reset_password_email').value;
		if (email === '') {
			document.getElementById('reset_password_email').style.border = '1px solid red';
		}
		axios
			.post('/reset_password', { email: email })
			.then((response) => {
				if (response.status === 200) {
					this.setState({ reset_password_success: true });
				}
			})
			.catch((err) => this.setState({ user_not_found: true }));
	};

	loginSuccess = () => {
		document.getElementById('form').style.display = 'none';
		document.getElementById('login_success').style.display = 'block';
	};

	setError = () => {
		const errors = this.state.errors;
		if (errors.email !== undefined) {
			document.getElementById('email_login').style.border = '1px solid red';
			document.getElementById('email_login_error').innerText = errors.email;
		} else {
			document.getElementById('email_login').style.border = '1px solid grey';
			document.getElementById('email_login_error').innerText = '';
		}
		if (errors.password !== undefined) {
			document.getElementById('password_login').style.border = '1px solid red';
			document.getElementById('password_login_error').innerText = errors.password;
		} else {
			document.getElementById('password_login').style.border = '1px solid grey';
			document.getElementById('password_login_error').innerText = '';
		}
		if (errors.user !== undefined) {
			document.getElementById('no_user').innerText = errors.user;
			document.getElementById('no_user').style.display = 'block';
		} else {
			document.getElementById('no_user').innerText = '';
			document.getElementById('no_user').style.display = 'none';
		}
	};

	render() {
		document.title = 'ចូលកម្មវិធី | ចង់និយាយថា';
		if (this.state.login_success) {
			return <Redirect to="/" />;
		}
		return (
			<div className="container">
				<div id="button">
					<Link to="/login" id="login1">
						ចូលកម្មវិធី
					</Link>
					<Link to="/register" id="register1">
						ចុះឈ្មោះ
					</Link>
				</div>
				<p id="no_user" />
				<form onSubmit={this.login} id="form">
					<div className="input">
						<label htmlFor="username">
							<strong>អ៊ីម៉ែល</strong>
						</label>

						<input id="email_login" name="email" type="text" onChange={this.onChange} />
						<p id="email_login_error" />
					</div>
					<div className="input">
						<label htmlFor="username">
							<strong>លេខសម្ងាត់</strong>
						</label>

						<input id="password_login" name="password" type="password" onChange={this.onChange} />
						<p id="password_login_error" />
					</div>

					<input id="submit" type="submit" value="ចូលកម្មវិធី" />

					<p>
						មិនទាន់មានគណនី?{'   '}
						<span>
							<Link id="create_account" to="/register">
								{' '}
								បង្កើតមួួយ!
							</Link>
						</span>
					</p>
					<div>
						ភ្លេចលេខសម្ងាត់?{' '}
						<span>
							<Popup trigger={<span id="forget_password">ផ្ញើរលេខសម្ងាត់</span>} modal>
								{this.state.reset_password_success && (
									<p className="reset_password_success_notice">
										សូមចូលទៅកាន់តំណដែលពួកយើងបានផ្ញើរទៅកាន់អុីម៉ែលរបស់អ្នក។
									</p>
								)}
								{this.state.user_not_found && (
									<p className="reset_password_success_notice">
										សូមអភ័យទោស! អុីម៉ែលនេះមិនមានគណនីនៅកម្មវិធីយើងទេ។
										<br />
										<span>សូមធ្វើការចុះឈ្មោះដើម្បីប្រើប្រាស់កម្មវិធី។</span>
									</p>
								)}

								{!this.state.reset_password_success &&
								!this.state.user_not_found && (
									<React.Fragment>
										<input id="reset_password_email" type="email" placeholder="email" />
										<button id="reset_password" onClick={this.resetPassword}>
											ផ្ញើរលេខសម្ងាត់ទៅកាន់អុីម៉ែល
										</button>
									</React.Fragment>
								)}
							</Popup>
						</span>{' '}
					</div>
				</form>

				<div id="login_success">
					<h3>
						<i className="fas fa-check" />
						ស្វាគមន៏មកកាន់កម្មវិធី ចង់និយាយថា!
					</h3>
					<h5>អ្នកអាចចាប់ផ្តើមចែករំលែកជាមួយពិភពលោកដោយការសរសេរអត្ថបទហើយនៅពេលនេះ!</h5>
				</div>
			</div>
		);
	}
}

export default Login;

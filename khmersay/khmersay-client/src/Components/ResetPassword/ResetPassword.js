import React, { Component } from 'react';
import axios from 'axios';
import { BounceLoader } from 'react-spinners';
import './ResetPassword.css';

class ResetPassword extends Component {
	state = {
		password_match: true,
		password_length: true,
		password: '',
		reset_success: false,
		server_error: false,
		loading: false,
		confirm_password: ''
	};
	set_error = () => {
		document.getElementById('password_input').style.border = '1px solid red';
		document.getElementById('confirm_password_input').style.border = '1px solid red';
	};
	reset_password = (e) => {
		e.preventDefault();
		this.setState({ loading: true });
		if (this.state.password !== this.state.confirm_password) {
			this.setState({ password_match: false });
			this.set_error();
		} else if (this.state.password.length < 6) {
			this.setState({ password_length: false });
			this.set_error();
		} else {
			axios
				.post('/set_password', {
					email: this.props.match.params.email,
					password: this.state.password
				})
				.then((response) => {
					this.setState({ loading: false });
					this.setState({ reset_success: true });
				})
				.catch((error) => {
					if (!error.status) {
						this.setState({ server_error: true });
					}
				});
		}
	};
	password = (e) => {
		this.setState({ password_length: true, password_match: true });
		this.setState({ password: e.target.value });
	};
	confirm_password = (e) => {
		this.setState({ password_length: true, password_match: true });
		this.setState({ confirm_password: e.target.value });
	};
	render() {
		if (this.state.loading) {
			return (
				<div id="loading_container">
					<BounceLoader color={'#369e59'} size={60} />
				</div>
			);
		}
		if (this.state.reset_success) {
			return (
				<div id="reset_success" className="container">
					<p>
						<span className="icon">
							<i className="far fa-hand-spock" />
						</span>អ្នកបានផ្លាស់ប្តូរលេខសម្ងាត់ដោយជោគជ័យ។
					</p>
					<p>អ្នកអាចចូលទៅកាន់កម្មវិធីដោយប្រើប្រាស់លេខសម្ងាត់ថ្មីរបស់អ្នក។</p>
				</div>
			);
		} else if (this.state.server_error) {
			return (
				<div id="server_error" className="container">
					<p>
						<span className="icon">
							<i className="fas fa-car-crash" />
						</span>{' '}
						Server កំពុងជួបបញ្ហា។
					</p>
					<p>សូមអ្នកព្យាយាមម្តងទៀតនៅ ៥ ទៅ ១០ នាទីក្រោយ</p>
				</div>
			);
		} else {
			return (
				<div id="new_password">
					<h2>Reset Password</h2>
					<form onSubmit={this.reset_password}>
						<input
							type="password"
							placeholder="New Password"
							className="password_input"
							id="password_input"
							onChange={this.password}
						/>
						<br />
						<input
							type="password"
							placeholder="Confirm Password"
							className="password_input"
							id="confirm_password_input"
							onChange={this.confirm_password}
						/>
						{!this.state.password_match && <p className="error">លេខសម្ងាត់ត្រូវតែដូចគ្នា!</p>}
						{!this.state.password_length && <p className="error">លេខសម្ងាត់ត្រូវមានយ៉ាងតិច៦អក្សរ</p>}
						<br />

						<input type="submit" text="Submit" id="submit_newpassword" />
					</form>
				</div>
			);
		}
	}
}

export default ResetPassword;

import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import logo from '../../assets/me.jpg';
import './Navbar.css';

class Navbar extends Component {
	state = {
		open_menu: false
	};
	open_menu = () => {
		if (!this.state.open_menu) {
			document.getElementById('side_menu').style.display = 'flex';
			this.setState({ open_menu: !this.state.open_menu });
		} else {
			document.getElementById('side_menu').style.display = 'none';
			this.setState({ open_menu: !this.state.open_menu });
		}
	};

	render() {
		return (
			<div>
				<nav className="navbar navbar-expand-lg navbar-light bg-light">
					<div className="container">
						<Link className="navbar-brand" to="/">
							<img id="logo" src={logo} alt="Logo" /> ចង់និយាយថា
						</Link>

						<div className="float-right">
							<ul className="navbar-nav">
								<li className="nav-item-1 nav-item">
									<Link to="/newpost">សរសេរអត្ថបទ</Link>
								</li>
								{!this.props.isLoggedIn && (
									<li className="nav-item">
										<Link to="/login">
											<i className="fas fa-sign-in-alt" /> ចូលកម្មវិធី
										</Link>
									</li>
								)}
								{!this.props.isLoggedIn && (
									<li className="nav-item">
										<Link to="/register">
											<i className="fas fa-user-plus" /> ចុះឈ្មោះ
										</Link>
									</li>
								)}
								{this.props.isLoggedIn && (
									<li className="nav-item" onClick={this.props.setLogout}>
										<Link to="/">
											<i className="fas fa-sign-out-alt" /> ចាកចេញ
										</Link>
									</li>
								)}
							</ul>

							<span id="menu_button" onClick={this.open_menu}>
								<span className="menu" />
								<span className="menu" />
								<span className="menu" />
							</span>
						</div>
					</div>
				</nav>

				<div id="side_menu" className="side_nav" onClick={this.open_menu}>
					<Link to="/">
						<i className="fas fa-home" /> ទំព័រដើម
					</Link>
					{!this.props.isLoggedIn && (
						<Link to="/login">
							{' '}
							<i className="fas fa-sign-in-alt" /> ចូលកម្មវិធី
						</Link>
					)}

					{!this.props.isLoggedIn && (
						<Link to="/register">
							<i className="fas fa-user-plus" /> ចុះឈ្មោះ
						</Link>
					)}
					{this.props.isLoggedIn && (
						<Link to="/newpost">
							<i className="fas fa-pencil-alt" /> សរសេអត្ថបទ
						</Link>
					)}
					{this.props.isLoggedIn && (
						<Link to="/setting">
							<i className="fas fa-cogs" /> កំណត់គណនី
						</Link>
					)}
					<Link to="/categories">
						<i className="fas fa-list-ul" /> ផ្នែក
					</Link>
					{this.props.isLoggedIn && (
						<Link to="/" onClick={this.props.setLogout}>
							<i className="fas fa-sign-out-alt" /> ចាកចេញ
						</Link>
					)}
				</div>
			</div>
		);
	}
}

export default Navbar;

import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Navbar from './Components/Navbar/Navbar';
import Login from './Components/Login/Login';
import Home from './Components/Home/Home';
import NewPost from './Components/NewPost/NewPost';
import Register from './Components/Register/Register';
import Setting from './Components/Setting/Setting';
import ResetPassword from './Components/ResetPassword/ResetPassword';
import ReadArticle from './Components/ReadArticle/ReadArticle';
import axios from 'axios';

import './App.css';

class App extends Component {
	state = {
		loggedIn: false,
		user: {}
	};

	setLogin = () => {
		this.setState({ loggedIn: true });
	};
	setLogout = () => {
		axios.get('/logout').then((response) => {
			this.setState({ loggedIn: false });
			this.setState({ user: {} });
		});
	};

	setUser = (user) => {
		this.setState({ user: user });
	};

	LogIn = (props) => {
		return <Login checkLogin={this.setLogin} setUser={this.setUser} />;
	};

	Home = (props) => {
		return <Home user={this.state.user} isLoggedIn={this.state.loggedIn} setLogout={this.setLogout} />;
	};

	NewPost = (props) => {
		return <NewPost user={this.state.user} isLoggedIn={this.state.loggedIn} setLogout={this.setLogout} />;
	};

	Setting = (props) => {
		return <Setting user={this.state.user} isLoggedIn={this.state.loggedIn} setLogout={this.setLogout} />;
	};

	ReadArticle = (props) => {
		return (
			<ReadArticle
				article={props.location.state}
				isLoggedIn={this.state.loggedIn}
				user={this.state.user}
				setLogout={this.setLogout}
			/>
		);
	};
	ResetPassword = (props) => {
		return <ResetPassword />;
	};

	render() {
		document.title = '| ខ្ញុំមានរឿងក្នុងចិត្តចង់និយាយ ... |';
		return (
			<Router>
				<div className="App">
					<Navbar isLoggedIn={this.state.loggedIn} setLogout={this.setLogout} />
					<Route path="/" exact render={this.Home} />
					<Route path="/login" exact render={this.LogIn} />
					<Route path="/register" exact component={Register} />
					<Route path="/newpost" exact render={this.NewPost} />
					<Route path="/setting" exact component={this.Setting} />
					<Route path="/article" exact render={this.ReadArticle} />
					<Route path="/reset_password/:email" component={ResetPassword} />
				</div>
			</Router>
		);
	}
}

export default App;

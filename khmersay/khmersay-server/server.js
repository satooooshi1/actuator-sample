const express = require('express'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	cookieParser = require('cookie-parser'),
	mongodbURI = require('./Config/config').mongodbURI,
	UserRoute = require('./Routes/UserRoute'),
	path = require('path'),
	ArticleRoute = require('./Routes/ArticleRoute'),
	cors = require('cors');
app = express();

//Database connection
mongoose.connect(mongodbURI).then(() => console.log('DB connected')).catch((err) => console.log(err));

//PORT
const PORT = process.env.PORT || 5000;

//cookie
app.use(cookieParser());

//cors
app.use(cors());

//body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//passport middle
app.use(passport.initialize());

//passort config
require('./Config/passport')(passport);

app.use(UserRoute);
app.use(ArticleRoute);

//Serve static asset in production
if (process.env.NODE_ENV === 'production') {
	app.use(express.static('khmersay-client/build'));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'khmersay-client', 'build', 'index.html'));
	});
}

app.listen(PORT, "0.0.0.0", (req, res) => {
	console.log(`Server started on port: ${PORT}`);
});

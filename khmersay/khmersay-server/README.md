# Khmersay 
From this project, I learned about:

1. CSS Flexbox, Bootstrap
2. ReactJS, states and props management
3. Client-Side routing using React-Router-Dom, the new and best practices on Redirect component
4. Deliberately try not to use Redux for state management and passing props between componets
5. Make http request to back-end API using Axios. 
6. Integrate third-party text Editor, TinyMCE
7. Design Database Schema in MongoDB
8. Server-Side routing
9. Build RESTful API in Nodejs 
10. Deploy to Heroku
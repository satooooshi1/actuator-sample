const express = require('express');
const ArticleModel = require('../Model/Article');
const UserModel = require('../Model/User');
const router = express.Router();

router.get('/articles', (req, res) => {
	ArticleModel.find({}, (err, articles) => {
		if (err) throw err;
		res.send(articles);
	});
});

router.get('/article/:id', (req, res) => {
	ArticleModel.findById(req.param.id, (err, article) => {
		if (err) throw err;
		res.send(article);
	});
});

router.post('/newpost', (req, res) => {
	const newpost = {
		title: req.body.title,
		article: req.body.content,
		author: {
			id: req.body.id,
			author_name: req.body.author_name
		},
		category: req.body.category,
		image: req.body.image,
		like: 0,
		read: 0
	};
	ArticleModel.create(newpost, (err, newpost) => {
		if (err) {
			console.log(err);
		} else {
			res.json(newpost);
		}
	});
});

router.post('/like', (req, res) => {
	const id = req.body.id;
	ArticleModel.findOneAndUpdate({ _id: id }, { $inc: { like: 1 } })
		.then((article) => res.json(article))
		.catch((err) => console.log(err));
});

router.post('/read', (req, res) => {
	const id = req.body.id;
	ArticleModel.findOneAndUpdate({ _id: id }, { $inc: { read: 1 } })
		.then((article) => res.json(article))
		.catch((err) => console.log(err));
});

router.post('/unlike', (req, res) => {
	const id = req.body.id;
	ArticleModel.findOneAndUpdate({ _id: id }, { $inc: { like: -1 } })
		.then((article) => res.json(article))
		.catch((err) => console.log(err));
});

module.exports = router;

const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UserModel = require('../Model/User');
const gravatar = require('gravatar');
const secretKey = require('../Config/config').secretKey;
const router = express.Router();
const passport = require('passport');
const sgMail = require('@sendgrid/mail');
const sendgridapi = require('../Config/config').sendgridapi;
//validation
const validateRegisterInput = require('../Validation/Register');
const validateLoginInput = require('../Validation/Login');

router.post('/reset_password', (req, res) => {
	let email = req.body.email;
	sgMail.setApiKey(sendgridapi);
	const msg = {
		to: email,
		from: 'no-reply@jorngniyaytha.com',
		subject: 'Password Reset on ចង់និយាយថា',
		text: 'and easy to do anywhere, even with Node.js',
		html: '<p>Password Reset</p>',
		templateId: 'd-018c5dd812a04997baecc032d52abfcb',
		personalizations: [
			{
				to: email,
				dynamic_template_data: {
					url: 'https://khmersay.herokuapp.com/reset_password/' + email
				}
			}
		]
	};
	UserModel.findOne({ email: email }).then((user) => {
		if (user) {
			sgMail.send(msg).then(() => res.status(200).send('ok')).catch((err) => console.error(err.toString()));
		} else {
			res.status(404).send('User not found');
		}
	});
});

router.post('/set_password', (req, res) => {
	UserModel.findOne({ email: req.body.email }).then((user) => {
		if (user) {
			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(req.body.password, salt, (err, hash) => {
					if (err) {
						err.user = 'អ្វីមួយមិនប្រក្រតី!';
						return res.status(500).json(err);
					}
					user.password = hash;
					//save new user to database
					user
						.save()
						.then((newUser) => {
							res.json(newUser);
						})
						.catch((err) => {
							res.json({ err: 'error' });
						});
				});
			});
		}
	});
});

router.post('/register', (req, res) => {
	const { err, valid } = validateRegisterInput(req.body);
	if (!valid) {
		return res.status(400).json(err);
	}
	UserModel.findOne({ email: req.body.email }).then((user) => {
		if (user) {
			err.user = 'មិនអាចបង្កើតគណនីនេះបានទេ';
			return res.status(404).json(err);
		} else {
			//get avatar associated with email
			const avatar = gravatar.url(req.body.email, {
				s: '200', //Size
				r: 'pg', //Rating
				d: 'mm' //Default
			});

			//new user object
			const newUser = new UserModel({
				username: req.body.username,
				email: req.body.email,
				avatar,
				password: req.body.password
			});

			//encrypt user password
			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(req.body.password, salt, (err, hash) => {
					if (err) {
						err.user = 'អ្វីមួយមិនប្រក្រតី!';
						return res.status(500).json(err);
					}
					newUser.password = hash;
					//save new user to database
					newUser.save().then((newUser) => res.json(newUser)).catch((err) => console.log(err));
				});
			});
		}
	});
});

router.post('/login', (req, res) => {
	const { err, valid } = validateLoginInput(req.body);
	if (!valid) {
		return res.status(400).send(err);
	}
	const email = req.body.email;
	const password = req.body.password;
	UserModel.findOne({ email: email }).then((user) => {
		if (!user) {
			err.user = 'គ្មានគណនីនេះទេ';
			res.status(404).json(err);
		} else {
			bcrypt.compare(password, user.password).then((isMatched) => {
				if (isMatched) {
					//user matched
					const payload = { username: user.username, id: user._id, avatar: user.avatar }; //Create JWT payload
					//sign token
					jwt.sign(payload, secretKey, { expiresIn: 60 * 60 }, (err, token) => {
						res.json({
							success: true,
							token: 'Bearer ' + token
						});
					});
				} else {
					err.user = 'បរាជ័យ! អ៊ីម៉ែលឬលេខសម្ងាត់មិនត្រឹមត្រូវ';
					res.status(400).json(err);
				}
			});
		}
	});
});

router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/');
});

router.get('/profile/:id', (req, res) => {
	UserModel.findOne({ id: req.param.id }).then((user) => {
		if (user) {
			user.password = '';
			res.json(user);
		} else {
			res.status(404).json({ err: 'Profile Not Found' });
		}
	});
});

module.exports = router;

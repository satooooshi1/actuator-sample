const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: String,
	email: String,
	password: String,
	avatar: String,
	article_number: Number,
	article: {
		type: Schema.Types.ObjectId,
		ref: 'articles'
	},
	read: Number,
	like: Number
});

module.exports = User = mongoose.model('users', UserSchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
	title: String,
	article: String,
	author: {
		id: {
			type: Schema.Types.ObjectId,
			ref: 'users'
		},
		author_name: String
	},
	comment: {
		type: Schema.Types.ObjectId,
		ref: 'comments'
	},
	category: String,
	image: String,
	like: Number,
	read: Number
});

module.exports = Article = mongoose.model('articles', ArticleSchema);

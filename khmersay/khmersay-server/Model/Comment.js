const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
	comment: String,
	commenter: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}
});

module.exports = Comment = mongoose.model('comments', CommentSchema);

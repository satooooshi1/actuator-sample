const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = validateLoginInput = (data) => {
	let err = {};

	data.email = !isEmpty(data.email) ? data.email : '';
	data.password = !isEmpty(data.password) ? data.password : '';

	if (!validator.isEmail(data.email)) {
		err.email = 'អ៊ីម៉ែលមិនត្រឹមត្រូវតាមទម្រង់';
	}

	if (validator.isEmpty(data.email)) {
		err.email = 'អ៊ីម៉ែលមិនអាចគ្មានទេ';
	}

	if (validator.isEmpty(data.password)) {
		err.password = 'លេខសម្ងាត់មិនអាចគ្មានទេ';
	}

	return {
		err: err,
		valid: isEmpty(err)
	};
};

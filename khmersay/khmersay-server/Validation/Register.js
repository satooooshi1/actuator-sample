const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = validateRegisterInput = (data) => {
	let err = {};

	data.username = !isEmpty(data.username) ? data.username : '';
	data.email = !isEmpty(data.email) ? data.email : '';
	data.password = !isEmpty(data.password) ? data.password : '';
	data.password2 = !isEmpty(data.password2) ? data.password2 : '';

	if (!validator.isLength(data.username, { min: 3, max: 30 })) {
		err.username = 'ឈ្មោះត្រូវមានយ៉ាងតិចពីរតួរនិងយ៉ាងច្រើន៣០តួរ ​​​ជាអក្សរអង់គ្លេស';
	}

	if (validator.isEmpty(data.username)) {
		err.username = 'ឈ្មោះមិនអាចគ្មានទេ';
	}

	if (!validator.isEmail(data.email)) {
		err.email = 'អ៊ីម៉ែលមិនត្រឹមត្រូវតាមទម្រង់';
	}

	if (validator.isEmpty(data.email)) {
		err.email = 'អ៊ីម៉ែលមិនអាចគ្មានទេ';
	}

	if (!validator.isLength(data.password, { min: 6, max: 30 })) {
		err.password = 'លេខសម្ងាត់ត្រូវមានយ៉ាងតិច៦តួរ';
	}

	if (validator.isEmpty(data.password)) {
		err.password = 'លេខសម្ងាត់មិនអាចគ្មានទេ';
	}

	if (validator.isEmpty(data.password2)) {
		err.password2 = 'លេខសម្ងាត់បញ្ជាក់មិនអាចគ្មានទេ';
	}

	if (!validator.equals(data.password, data.password2)) {
		err.password2 = 'លេខសម្ងាត់ត្រូវតែដូចគ្នា';
	}

	return {
		err: err,
		valid: isEmpty(err)
	};
};

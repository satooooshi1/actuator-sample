module.exports = {
	mongodbURI: process.env.MONGO_URI,
	secretKey: process.env.SECRET_KEY,
	sendgridapi: process.env.SENDGRID_API
};

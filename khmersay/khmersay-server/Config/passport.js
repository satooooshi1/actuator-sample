const jwtStrategy = require('passport-jwt').Strategy,
	ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const UserModel = mongoose.model('users');
const secretKey = require('./config').secretKey;

const opt = {};

opt.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opt.secretOrKey = secretKey;

module.exports = (passport) => {
	passport.use(
		new jwtStrategy(opt, (jwt_payload, done) => {
			UserModel.findById(jwt_payload.id)
				.then((user) => {
					if (user) {
						return done(null, user);
					} else {
						return done(null, false);
					}
				})
				.catch((err) => console.log(err));
		})
	);
};

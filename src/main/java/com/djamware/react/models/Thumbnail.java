package com.djamware.react.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "thumbnail")
public class Thumbnail {
    @Id
    String id;
    String title;
    String content;
    String contactId;


    public Thumbnail() {
    }

    public Thumbnail(String id, String title, String content, String contactId) {
        this.id = id;
        this.title = title;
        this.content= content;
        this.contactId=contactId;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId)
    {
        this.contactId = contactId;
    }

    public String getContent(){return content;}
    public void setContent(String content){this.content=content;}
}

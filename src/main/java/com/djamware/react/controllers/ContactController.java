package com.djamware.react.controllers;

import com.djamware.react.models.Contact;
import com.djamware.react.repositories.ContactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
public class ContactController {

    @Autowired
    ContactRepository contactRepository;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ContactController.class);

    public void logEnvVars(){
        LOGGER.info("MONGODB_HOST_NAME: " + System.getenv("MONGODB_HOST_NAME"));
        LOGGER.info("MONGODB_DB_NAME: " + System.getenv("MONGODB_DB_NAME"));
        LOGGER.info("MONGODB_DB_PORT: " + System.getenv("MONGODB_DB_PORT"));
    }

    @RequestMapping(method=RequestMethod.GET, value="/hello")
    public String hello()
    {
        this.logEnvVars();
        return "Hello World!!";
    }

    @RequestMapping(method=RequestMethod.GET, value="/contacts")
    public Iterable<Contact> contact()
    {
        return contactRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/contacts")
    public Contact save(@RequestBody Contact contact) {
        System.out.println("Added:");
        System.out.println(contact);
        contactRepository.save(contact);
        return contact;
    }

    @RequestMapping(method=RequestMethod.GET, value="/contacts/{id}")
    public Optional<Contact> show(@PathVariable String id) {
        return contactRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.DELETE, value="/contacts/{id}")
    public String delete(@PathVariable String id) {
        Optional<Contact> optcontact = contactRepository.findById(id);
        Contact contact = optcontact.get();
        contactRepository.delete(contact);

        return "";
    }
}

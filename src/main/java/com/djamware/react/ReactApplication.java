package com.djamware.react;

import com.djamware.react.models.Contact;
import com.djamware.react.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactApplication implements CommandLineRunner/*for run function*/ {

	public static void main(String[] args) {
		SpringApplication.run(ReactApplication.class, args);

	}

	@Autowired
	private ContactRepository repository;

	@Override
	public void run(String... args) throws Exception {

		repository.deleteAll();

		// save a couple of customers
		repository.save(new Contact("Alice", "South china"));
		repository.save(new Contact("Nicole", "France"));
		repository.save(new Contact("John", "Russia"));
		repository.save(new Contact("Zhang", "China"));
		repository.save(new Contact("Satoshi", "Japan"));
		repository.save(new Contact("Hokhy", "Cambodia"));

		// fetch all customers
		System.out.println("Contacts found with findAll():");
		System.out.println("-------------------------------");
		for (Contact contact : repository.findAll()) {
			System.out.println(contact);
		}
		System.out.println("-------------------------------");


	}
}

package com.djamware.react.repositories;

import com.djamware.react.models.Contact;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends MongoRepository<Contact, String> {

    List<Contact> findAll();
    Contact save(Contact contact);
    Optional<Contact> findById(String id);
    @Override
    void delete(Contact deleted);

}
